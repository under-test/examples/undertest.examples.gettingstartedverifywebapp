using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium;

namespace UnderTest.Examples.VerifyWebApp.FeatureHandlers
{
  [ExcludeFromCodeCoverage]
  [HandlesFeature("AboutPage.feature")]
  public class AboutPageFeatureHandler : SeleniumFeatureHandler
  {
    const string AboutPage = "https://thinkingbig.net/about/";

    [When("I visit the About page")]
    public void IVisitTheAboutPage()
    {
      CurrentDriver.Navigate().GoToUrl(AboutPage);
    }

    [Then("the current page should be the About page")]
    public void TheCurrentPageIsTheAboutPage()
    {
      CurrentDriver.Url.Should().Be(AboutPage);
    }

    [Then("I should see the Thinking Big Executives")]
    public void ThenIShouldSeeTBExecs()
    {
      this.FindPageElements(".executive")
          .Should()
          .HaveCountGreaterThan(0);
    }

    [Then("I should see the Thinking Big Team")]
    public void ThenIShouldSeeTBTeam()
    {
      this.FindPageElements(".team-member")
          .Should()
          .HaveCountGreaterThan(0);
    }

    [Then("I should see the Thinking Big Contact Information")]
    public void ThenIShouldSeeTBContactInfo()
    {
      this.FindPageElement("#contact_footer")
          .ShouldExist();
    }
  }
}
