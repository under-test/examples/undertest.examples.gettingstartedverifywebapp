using System;
using System.Diagnostics.CodeAnalysis;
using OpenQA.Selenium.Chrome;
using UnderTest;
using UnderTest.Strategy.Selenium;

namespace UnderTest.Examples.VerifyWebApp
{
  [ExcludeFromCodeCoverage]
  class Program
  {
    static int Main(string[] args)
    {
      var assembly = typeof(Program).Assembly;
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest Getting Started")
          .SetProjectVersion("0.1.0"))
        .WithTestSettings(settings => settings
          .AddAssembly(assembly)
          .AddSeleniumStrategy(strategy => strategy
            .WithFeatureHandlersFrom(assembly)
            .SetDriverCreationFunc(() => new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory))
            .SetDriverLifeCycle(DriverLifecycle.EntireRun)
            .WithWebsiteComponentsFrom(assembly))
          .AttachBehaviors()
          .CaptureScreenshots()
          .FinishAttaching())
        .Execute()
          .ToExitCode();
    }
  }
}
