Feature: About Page listing team members

  As a visitor to ThinkingBig.net
  I want to be able to see who works at ThinkingBig
  In order to understand the team composition

  Scenario: User visits the Tb About page
    When I visit the About page
    Then the current page should be the About page
    And I should see the Thinking Big Executives
    And I should see the Thinking Big Team
    And I should see the Thinking Big Contact Information
