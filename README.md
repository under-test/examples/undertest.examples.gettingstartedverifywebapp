# UnderTest.Examples.GettingStartedVerifyWebApp

This is a basic example focused on getting started with UnderTest to build an acceptance test suite.

> TLDR: In this example, using UnderTest, we are going to visit https://thinkingbig.net/about and verify the people on it.  This setup is an analogue to many BDD scenarios we will add to many of our applications.  After this is complete, we will have a basic working test suite that we can build on.

## Example

In this project, we will be verifying a web application/page as our acceptance criteria outlines — specifically the [Thinking Big about page](https://thinkingbig.net/about/).

For this use case, we have drafted a feature with our [Three Amigos](https://automationpanda.com/2017/02/20/the-behavior-driven-three-amigos/).

For our current version of this requirement, the team has drafted the following feature:

```
Feature: About Page listing team members

  As a visitor to Thinking Big's site
  I want to be able to see who works at ThinkingBig
  In order to understand the team composition

  Scenario: User visits the TB About page
    When I visit the About page
    Then the current page should be the About page
      And I should see the Thinking Big Executives
      And I should see the Thinking Big Team
      And I should see the Thinking Big Contact Information
```

With that in hand, let's get started.

## Prerequisites

* [.net core 2.2+](https://dotnet.microsoft.com/download/dotnet-core/2.2)

## Getting Started

1. Open a command-line window in the folder you wish to create the project.  Preferably an empty one.

1. Ensure you have installed UnderTest.Templates Installed.

    ```
    dotnet new -i UnderTest.Templates
    ```
1. Create your project

    ```
    dotnet new UnderTest --name "UnderTest.Examples.SimpleVerifyWebApp"
    ```

    > replace the name for your project if you are using this to jump-start your project.

    The above command creates an UnderTest project with the bones of what we need to create our acceptance suite.

    If we `cd` and `ls` to the project we see

    ![getting-started](./docs/img/getting-started1.gif)

1. Now we need to add two more pieces 

    ```
    dotnet add package UnderTest.Strategy.Selenium --version 0.2.0-rc064
    dotnet add package UnderTest --version 0.8.0-rc031
    ```

    The [UnderTest.Strategy.Selenium](https://gitlab.com/under-test/undertest.strategy.selenium/blob/stable/README.md) package is our strategy for interacting with [Selenium](https://www.seleniumhq.org/).  

    And:

    ```
    dotnet add package Selenium.WebDriver.ChromeDriver
    ``` 

    This adds the [Chrome Web Driver](https://sites.google.com/a/chromium.org/chromedriver/downloads) (browser we can automate) to our project.  
    > Each of the major web browsers has their associated package.

1. Our project is now setup for us to build our test suite. Open the project in your favorite Dotnet dev tool (Rider, VSCode, Visual Studio, etc).

    > ![Our project in Rider](./docs/img/getting-started2.png)
    
1. Let's check out our `Program.cs`

    Our UnderTest project is a command-line application.  This will allow us to easily integrate our acceptance test project into our build pipeline.  Program.cs is our entry point into that command-line app.

    It will look something like:

    <details>
      <summary>Program.cs</summary>
    
    ```csharp
    using System.Diagnostics.CodeAnalysis;
    using UnderTest;

    namespace UnderTest.Examples.SimpleVerifyWebApp
    {
      [ExcludeFromCodeCoverage]
      class Program
      {
        static int Main(string[] args)
        {
          return new UnderTestRunner()
            .WithCommandLineArgs(args)
              .WithProjectDetails(x => x
              .SetProjectName("YOUR PROJECT NAME Test Suite")
              .SetProjectVersion("0.1.0"))
            .WithTestSettings(settings => settings
              .AddAssembly(typeof(Program).Assembly))
            .Execute()
              .ToExitCode();
        }
      }
    }

    ```
    
    </details>

    Let's update the project name to `UnderTest Getting Started`.

1. And update the program.cs to include our Selenium testing strategy

    >We are working on a template that includes this step but it is not ready for release yet.

    <details>
      <summary>Updated Program.cs</summary>
    
    ```csharp
    using System;
    using System.Diagnostics.CodeAnalysis;
    using OpenQA.Selenium.Chrome;
    using UnderTest;
    using UnderTest.Strategy.Selenium;

    namespace UnderTest.Examples.SimpleVerifyWebApp
    {
      [ExcludeFromCodeCoverage]
      class Program
      {
        static int Main(string[] args)
        {
          var assembly = typeof(Program).Assembly;
          return new UnderTestRunner()
            .WithCommandLineArgs(args)
            .WithProjectDetails(x => x
              .SetProjectName("UnderTest Getting Started")
              .SetProjectVersion("0.1.0"))
            .WithTestSettings(settings => settings
              .AddAssembly(assembly)
              .AddSeleniumStrategy(strategy => strategy
                .WithFeatureHandlersFrom(assembly)
                .SetDriverCreationFunc(() => new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory))
                .SetDriverLifeCycle(DriverLifecycle.EntireRun)
                .WithWebsiteComponentsFrom(assembly))
              .AttachBehaviors()
              .CaptureScreenshots()
              .FinishAttaching())
            .Execute()
            .ToExitCode();
        }
      }
    }
    ```
    
    </details>


1. Remove the example Feature and FeatureHandler, we will create our own.

   (feel free to read these before deleting them, but we don't need them for this project)

   * delete `Features/SimpleFeature.feature`
   * delete `FeatureHandlers/SimpleFeatureHandler.cs`

1. Add our Feature

    * Add a new file `AboutPage.feature` to the `Features` folder.
    * Add the contents of the feature above to this file:

    <details>
      <summary>AboutPage.feature</summary>
        
    ```gherkin
    Feature: About Page listing team members

    As a visitor to ThinkingBig.net
    I want to be able to see who works at ThinkingBig
    In order to understand the team composition

    Scenario: User visits the Tb About page
        When I visit the About page
        Then the current page should be the About page
        And I should see the Thinking Big Executives
        And I should see the Thinking Big Team
        And I should see the Thinking Big Contact Information
    ```

    </details>

    At this point, you can run the project and see the output.

    The output should look like the output below (depending on the version you have installed):

    <details>
      <summary>Incomplete log output</summary>
    <pre>
    [19:20:00 INF] Running preflight checks
    [19:20:00 INF] Preflight checks complete
    [19:20:00 INF] 
    [19:20:00 INF] Debug assembly detected. Running [DebugRunThis] filter.
    [19:20:00 INF] No [DebugRunThis] found. Running [DebugIgnoreThis] filter.
    [19:20:00 INF] No [DebugIgnoreThisAttribute] marked classes found.
    [19:20:00 INF] 
    [19:20:00 INF] UnderTest version 0.6.0
    [19:20:00 INF] 
    [19:20:00 INF] Project details:
    [19:20:00 INF] ----------------
    [19:20:00 INF]   Project name: UnderTest Getting Started
    [19:20:00 INF] 
    [19:20:00 INF] Test settings:
    [19:20:00 INF] -------------
    [19:20:00 INF]   Assemblies:
    [19:20:00 INF]     C:\temp\getting-started\UnderTest.Examples.SimpleVerifyWebApp\bin\Debug\netcoreapp2.2\UnderTest.Examples.SimpleVerifyWebApp.dll
    [19:20:00 INF]   Strategies:
    [19:20:00 INF]     Default Global Strategy
    [19:20:00 INF]   Step Locator: Default Step Locator
    [19:20:00 INF]   Container Locator: Default FeatureHandler Locator

    [19:20:00 INF] Features: 
    [19:20:00 INF] ---------
    [19:20:00 INF]   Testable: 1
    [19:20:00 INF]        Wip: 0
    [19:20:00 INF]    Ignored: 0

    [19:20:00 INF] 
    [19:20:00 INF] ╔═══════════════════╗
    [19:20:00 INF] ║ AboutPage.feature ║
    [19:20:00 INF] ╚═══════════════════╝
    [19:20:00 WRN]   No handler found for feature AboutPage.feature. We will attempt to look for global handlers.
    [19:20:00 INF] Executing scenario: ==> User visits the Tb About page
    [19:20:00 INF]   Executing step 'When I visit the About page'
    [19:20:00 WRN] Unable to locate method for step 'When I visit the About page'
    [19:20:00 ERR] Missing step binding 'When I visit the About page' - this feature will be marked as inconclusive.
    [19:20:00 INF]   Skipping step 'Then the current page should be the About page' as there was a previous failure
    [19:20:00 INF]   Skipping step 'And I should see the Thinking Big Executives' as there was a previous failure
    [19:20:00 INF]   Skipping step 'And I should see the Thinking Big Team' as there was a previous failure
    [19:20:00 INF]   Skipping step 'And I should see the Thinking Big Contact Information' as there was a previous failure
    [19:20:00 WRN] ##--------> Scenario result: inconclusive

    [19:20:00 INF] 
    [19:20:00 INF] Test Run Results:
    [19:20:00 INF] ----------------
    [19:20:00 INF]   Overall result: Inconclusive
    [19:20:00 INF]   Features processed: 1
    [19:20:00 INF]               Passed: 0
    [19:20:00 INF]               Failed: 0
    [19:20:00 INF]         Inconclusive: 1
    [19:20:00 INF]                  Wip: 0
    [19:20:00 INF]              Skipped: 0
    [19:20:00 INF] 
    [19:20:00 INF]   Scenarios processed: 1
    [19:20:00 INF]                Passed: 0
    [19:20:00 INF]                Failed: 0
    [19:20:00 INF]          Inconclusive: 1
    [19:20:00 INF]                   Wip: 0
    [19:20:00 INF]               Skipped: 0
    [19:20:00 INF]        Execution time: 0.612 seconds
    [19:20:00 INF]            Start time: 2019-11-09 11:19:59 PM (UTC)
    [19:20:00 INF]              End time: 2019-11-09 11:20:00 PM (UTC)
    [19:20:00 INF] 
    [19:20:00 INF] Results saved to: C:\temp\getting-started\UnderTest.Examples.SimpleVerifyWebApp\bin\Debug\netcoreapp2.2\reports\undertest-result.json
    </pre>
    </details>

    the important part here is:

    <details>
      <summary>Log output</summary>
    <pre>
    [19:20:00 INF]   Executing step 'When I visit the About page'
    [19:20:00 WRN] Unable to locate method for step 'When I visit the About page'
    [19:20:00 ERR] Missing step binding 'When I visit the About page' - this feature will be marked as inconclusive.
    </pre>
    </details>

    `Unable to locate method for step 'When I visit the About page'` means UnderTest doesn't know how to execute that portion of the test. This is where our `FeatureHandlers` come into play.  Let's add one now.

1. Add our FeatureHandler

    * Add a new file `AboutPageFeatureHandler.cs` to the `FeatureHandlers` folder.
    * Add the below contents to this file:

    <details>
      <summary>FeatureHandlers/AboutPageFeatureHandler.cs</summary>
    
    ```csharp
    using System.Diagnostics.CodeAnalysis;
    using FluentAssertions;
    using UnderTest.Attributes;
    using UnderTest.Strategy.Selenium;

    namespace UnderTest.Examples.VerifyWebApp.FeatureHandlers
    {
      [ExcludeFromCodeCoverage]
      [HandlesFeature("AboutPage.feature")]
      public class AboutPageFeatureHandler : SeleniumFeatureHandler
      {
        const string AboutPage = "https://thinkingbig.net/about/";

        [When("I visit the About page")]
        public void IVisitTheAboutPage()
        {
          CurrentDriver.Navigate().GoToUrl(AboutPage);
        }

        [Then("the current page should be the About page")]
        public void TheCurrentPageIsTheAboutPage()
        {
          CurrentDriver.Url.Should().Be(AboutPage);
        }

        [Then("I should see the Thinking Big Executives")]
        public void ThenIShouldSeeTBExecs()
        {
          this.FindPageElements(".executive")
              .Should()
              .HaveCountGreaterThan(0);
        }

        [Then("I should see the Thinking Big Team")]
        public void ThenIShouldSeeTBTeam()
        {
          this.FindPageElements(".team-member")
              .Should()
              .HaveCountGreaterThan(0);
        }

        [Then("I should see the Thinking Big Contact Information")]
        public void ThenIShouldSeeTBContactInfo()
        {
          this.FindPageElement("#contact_footer")
              .ShouldExist();
        }
      }
    }
    ```    
    </details>

    there are several important details here:

      1. `[HandlesFeature("AboutPage.feature")]` wires our `AboutPage.feature` to this `FeatureHandler` in a way that UnderTest knows how to tie them together.
      1. The class inherits from `SeleniumFeatureHandler` to add in support for a Selenium based FeatureHandler.
      1. `[When("I visit the About page")]` wires the step in our feature file to this method.  There are `Given`, `When`, `Then` attributes to tied each of those types of steps and `Step` which will tie to any of the step types.  
      1. That's it.  Your UnderTest project should be setup.  
  
And our logs should look like:

  <details>
    <summary>Expected Log Output</summary>
  <pre>

[21:04:33 INF] Running preflight checks
[21:04:33 INF] Preflight checks complete
[21:04:33 INF]
[21:04:33 INF] Debug assembly detected. Running [DebugRunThis] filter.
[21:04:33 INF] No [DebugRunThis] found. Running [DebugIgnoreThis] filter.
[21:04:33 INF] No [DebugIgnoreThisAttribute] marked classes found.
[21:04:33 INF]
[21:04:33 INF] UnderTest version 0.8.0-rc031
[21:04:33 INF]
[21:04:33 INF] Project details:
[21:04:33 INF] ----------------
[21:04:33 INF]   Project name: UnderTest Getting Started
[21:04:33 INF]
[21:04:33 INF] Test settings:
[21:04:33 INF] -------------
[21:04:33 INF]   Assemblies:
[21:04:33 INF]     C:\dev\OSS\under-test\undertest.examples.gettingstartedverifywebapp\src\bin\Debug\netcoreapp2.2\UnderTest.Examples.GettingStartedVerifyWebApp.dll
[21:04:33 INF]   Strategies:
[21:04:33 INF]     Web browser testing
[21:04:33 INF]   Step Locator: Default Step Locator
[21:04:33 INF]   Container Locator: Default FeatureHandler Locator

[21:04:33 INF] Features:
[21:04:33 INF] ---------
[21:04:33 INF]   Testable: 1
[21:04:33 INF]        Wip: 0
[21:04:33 INF]    Ignored: 0

[21:04:33 INF]
[21:04:33 INF] ╔═══════════════════╗
[21:04:33 INF] ║ AboutPage.feature ║
[21:04:33 INF] ╚═══════════════════╝
[21:04:33 INF]   Handler located class name is AboutPageFeatureHandler.
[21:04:33 INF] Executing scenario: ==> User visits the Tb About page
[21:04:33 INF]   Executing step 'When I visit the About page'
Starting ChromeDriver 83.0.4103.39 (ccbf011cb2d2b19b506d844400483861342c20cd-refs/branch-heads/4103@{#416}) on port 60046
Only local connections are allowed.
Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
ChromeDriver was started successfully.

DevTools listening on ws://127.0.0.1:60049/devtools/browser/4144a74c-2faa-44d7-9513-6438c5ef3dac
[21:04:39 INF]   Executing step 'Then the current page should be the About page'
[21:04:40 INF]   Executing step 'And I should see the Thinking Big Executives'
[21:04:40 INF]   Executing step 'And I should see the Thinking Big Team'
[21:04:41 INF]   Executing step 'And I should see the Thinking Big Contact Information'
[21:04:42 INF] ##--------> Scenario result: passed

[21:04:42 INF]
[21:04:42 INF] Test Run Results:
[21:04:42 INF] ----------------
[21:04:42 INF]   Overall result: Pass
[21:04:42 INF]   Features processed: 1
[21:04:42 INF]               Passed: 1
[21:04:42 INF]               Failed: 0
[21:04:42 INF]         Inconclusive: 0
[21:04:42 INF]                  Wip: 0
[21:04:42 INF]              Skipped: 0
[21:04:42 INF]
[21:04:42 INF]   Scenarios processed: 1
[21:04:42 INF]                Passed: 1
[21:04:42 INF]                Failed: 0
[21:04:42 INF]          Inconclusive: 0
[21:04:42 INF]                   Wip: 0
[21:04:42 INF]               Skipped: 0
[21:04:42 INF]        Execution time: 8.699 seconds
[21:04:42 INF]            Start time: 2020-07-16 12:04:33 AM (UTC)
[21:04:42 INF]              End time: 2020-07-16 12:04:42 AM (UTC)
[21:04:42 INF]
[21:04:42 INF] Results saved to: C:\dev\OSS\under-test\undertest.examples.gettingstartedverifywebapp\src\bin\Debug\netcoreapp2.2\reports\2020-07-15-21-04-33\undertest-result.json
  </pre>
  </details>
  
  We can now add more scenarios, features and FeatureHandlers to add more breadth to our acceptance project.
  
If you have any issues, or would like to see another type of example [create an Issue](https://gitlab.com/under-test/examples/undertest.examples.gettingstartedverifywebapp/issues/new) and we will see if we can help. 
